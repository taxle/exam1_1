def series_sum(f):
    series_sum = 0.0
    #если делитель не равен нулю, то вычисляем сумму ряда, преобразуя строку в число
    for line in f:
        if (int(line) != 0):
            series_sum += 1.0 / int(line)
    series_sum += 42
    return series_sum