import series_module
import sys
list = []
#формируем список чисел из входных данных стандартного потока, куда перенаправлен файл
for line in sys.stdin:
	if(line.isnumeric):
		list.append(int(line))
#вызываем функцию подсчёта ряда
print(series_module.series_sum(list))